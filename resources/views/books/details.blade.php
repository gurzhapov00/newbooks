@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Просмотр сведений о книге</div>

                <div class="card-body">
                    <table class="table table-stribed">
                        <tr>
                            <th>Название</th>
                            <td class="lead"><b>{{ $book->title }}</b></td>
                        </tr>

                        <tr>
                            <th>Рейтинг пользователей</th>
                            <td>
                                <span class="score">
                                    <div class="score-wrap">
                                        <span class="stars-active" style="width: {{ $book->rate()*20 }}%">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>

                                        <span class="stars-inactive">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                    </div>
                                </span>
                                <span> Количество оценщиков {{ $book->ratings()->count() }} пользователей </span>
                            </td>
                        </tr>

                        @if ($book->isbn)
                            <tr>
                                <th>Серийный номер</th>
                                <td>{{ $book->isbn }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Фото на обложке</th>
                            <td>
                                @if(str_contains ($book->cover_image, 'app/public/images/covers'))
                                    <img class="img-fluid img-thumbnail" src="{{ asset('storage/' . $book->cover_image) }}" alt="">
                                @else
                                    <img class="img-fluid img-thumbnail" src="{{ $book->cover_image }}" alt="">
                                @endif
                            </td>
                        </tr>
                        @if ($book->category)
                            <tr>
                                <th>Категория</th>
                                <td>{{ $book->category->name }}</td>
                            </tr>
                        @endif
                        @if ($book->authors()->count() > 0)
                            <tr>
                                <th>Авторы</th>
                                <td>
                                    @foreach ($book->authors as $author)
                                        {{ $loop->first ? '' : 'و' }}
                                        {{ $author->name }}
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if ($book->publisher)
                            <tr>
                                <th>Издатель</th>
                                <td>{{ $book->publisher->name }}</td>
                            </tr>
                        @endif
                        @if ($book->description)
                            <tr>
                                <th>Описание</th>
                                <td>{{ $book->description }}</td>
                            </tr>
                        @endif
                        @if ($book->publish_year)
                            <tr>
                                <th>Год публикации</th>
                                <td>{{ $book->publish_year }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Количество страниц</th>
                            <td>{{ $book->number_of_pages }}</td>
                        </tr>
                        <tr>
                            <th>Количество копий</th>
                            <td>{{ $book->number_of_copies }}</td>
                        </tr>
                        <tr>
                            <th>Цена</th>
                            <td>{{ $book->price }} ₽</td>
                        </tr>
                    </table>
                    @auth
                        <h4>Оцените эту книгу<h4>
                        @if ($bookfind)
                            @if(auth()->user()->rated($book))
                                <div class="rating">
                                    <span class="rating-star {{ auth()->user()->bookRating($book)->value == 5 ? 'checked' : '' }}" data-value="5"></span>
                                    <span class="rating-star {{ auth()->user()->bookRating($book)->value == 4 ? 'checked' : '' }}" data-value="4"></span>
                                    <span class="rating-star {{ auth()->user()->bookRating($book)->value == 3 ? 'checked' : '' }}" data-value="3"></span>
                                    <span class="rating-star {{ auth()->user()->bookRating($book)->value == 2 ? 'checked' : '' }}" data-value="2"></span>
                                    <span class="rating-star {{ auth()->user()->bookRating($book)->value == 1 ? 'checked' : '' }}" data-value="1"></span>
                                </div>
                            @else
                                <div class="rating">
                                    <span class="rating-star" data-value="5"></span>
                                    <span class="rating-star" data-value="4"></span>
                                    <span class="rating-star" data-value="3"></span>
                                    <span class="rating-star" data-value="2"></span>
                                    <span class="rating-star" data-value="1"></span>
                                </div>
                            @endif
                        @else
                        <div class="alert alert-danger" role="alert">
                            Вы должны приобрести книгу, чтобы иметь возможность оценить ее
                        </div>
                        @endif
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('.rating-star').click(function() {

            var submitStars = $(this).attr('data-value');

            $.ajax({
                type: 'post',
                url: {{ $book->id }} + '/rate',
                data: {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'value' : submitStars
                },
                success: function() {
                    alert('Оценка успешно завершена');
                    location.reload();
                },
                error: function() {
                    alert('Что-то пошло не так');
                },
            });
        });
    </script>
@endsection
