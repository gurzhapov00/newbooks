@extends('theme.default')

@section('heading')
    Просмотр сведений о книге
@endsection

@section('head')
    <style>
        table {
            table-layout: fixed;
        }
        table tr th {
            width: 30%;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Просмотр сведений о книге</div>

                <div class="card-body">
                    <table class="table table-stribed">
                        <tr>
                            <th>Название</th>
                            <td class="lead"><b>{{ $book->title }}</b></td>
                        </tr>

                        @if ($book->isbn)
                            <tr>
                                <th>Серийный номер</th>
                                <td>{{ $book->isbn }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Фото на обложке</th>
                            <td>
                                @if(str_contains ($book->cover_image, 'app/public/images/covers'))
                                    <img class="img-fluid img-thumbnail" src="{{ asset('storage/' . $book->cover_image) }}" alt="">
                                @else
                                    <img class="img-fluid img-thumbnail" src="{{ $book->cover_image }}" alt="">
                                @endif
                            </td>
                        </tr>
                        @if ($book->category)
                            <tr>
                                <th>Категория</th>
                                <td>{{ $book->category->name }}</td>
                            </tr>
                        @endif
                        @if ($book->authors()->count() > 0)
                            <tr>
                                <th>Авторы</th>
                                <td>
                                    @foreach ($book->authors as $author)
                                        {{ $loop->first ? '' : 'و' }}
                                        {{ $author->name }}
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if ($book->publisher)
                            <tr>
                                <th>Издатель</th>
                                <td>{{ $book->publisher->name }}</td>
                            </tr>
                        @endif
                        @if ($book->description)
                            <tr>
                                <th>Описание</th>
                                <td>{{ $book->description }}</td>
                            </tr>
                        @endif
                        @if ($book->publish_year)
                            <tr>
                                <th>Год публикации</th>
                                <td>{{ $book->publish_year }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th>Количество страниц</th>
                            <td>{{ $book->number_of_pages }}</td>
                        </tr>
                        <tr>
                            <th>Количество копий</th>
                            <td>{{ $book->number_of_copies }}</td>
                        </tr>
                        <tr>
                            <th>Цена</th>
                            <td>{{ $book->price }}₽</td>
                        </tr>
                    </table>
                    <a class="btn btn-info btn-sm" href="{{ route('books.edit', $book) }}"><i class="fa fa-edit"></i> Редактировать</a>
                    <form class="d-inline-block" method="POST" action="{{ route('books.destroy', $book) }}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Уверены?')"><i class="fa fa-trash"></i> Удалить </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
