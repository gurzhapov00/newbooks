<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $authors = Author::all();
        return view('admin.authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|Redirector|RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $author = new Author;
        $author->name = $request->name;
        $author->description = $request->description;
        $author->save();

        session()->flash('flash_message',  'Автор успешно добавлен');

        return redirect(route('authors.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Author $author
     * @return Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Author $author
     * @return Application|Factory|View
     */
    public function edit(Author $author)
    {
        return view('admin.authors.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Author $author
     * @return Application|Redirector|RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Author $author)
    {
        $this->validate($request, ['name' => 'required']);

        $author->name = $request->name;
        $author->description = $request->description;
        $author->save();

        session()->flash('flash_message',  'Данные автора успешно изменены');

        return redirect(route('authors.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Author $author
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy(Author $author)
    {
        $author->delete();
        session()->flash('flash_message', 'Автор успешно удален');
        return redirect(route('authors.index'));
    }

    public function result(Author $author)
    {
        $books = $author->books()->paginate(12);
        $title = 'Книги автора: ' . $author->name;
        return view('gallery', compact('books', 'title'));
    }

    public function list()
    {
        $authors = Author::all()->sortBy('name');
        $title = 'Авторы';
        return view('authors.index', compact('authors', 'title'));
    }

    public function search(Request $request)
    {
        $authors = Author::where('name', 'like', "%{$request->term}%")->get()->sortBy('name');
        $title = 'Результаты поиска для: ' . $request->term;
        return view('authors.index', compact('authors', 'title'));
    }
}
