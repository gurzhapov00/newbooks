<?php
namespace App\Service;
use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Models\Publisher;
use App\Traits\ImageUploadTrait;

include_once 'D:\Soft\XAMPP\htdocs\storebooks\vendor\simple_html_dom.php';
class ParserService{
    use ImageUploadTrait;
    public function parser()
    {
        /*ImageUploadTrait::
        $image_trait = new ImageUploadTrait();*/
        $page = $this->curlGetPage('https://www.labirint.ru/books/827505/');
        $html = str_get_html($page);
        $title = explode(': ', $html->find('[class=prodtitle]', 0)->children(0)->plaintext, 2)[1];
        $description = $html->find('[id=smallannotation]', 0)->plaintext;
        $price = $html->find('[class=buying-price-val-number]', 0)->plaintext;
        $quantity = rand(15, 50);
        $image = $html->find('[class=book-img-cover]', 0)->getAttribute('data-src');
        $publisher = $html->find('[data-event-label=publisher]', 0)->plaintext;
        $idPublisher = Publisher::where('name', 'LIKE', "%".$publisher."%")->first()->id;

        $preYear = $html->find('[class=publisher]', 0)->plaintext;
        $yearStr = preg_replace ('/\s+/', '', $preYear);
        $year = substr($yearStr, strlen($yearStr)-7, 4);
        $genre = $html->find('[data-event-label=genre]',0)->plaintext;
        $idGenre = Category::where('name', 'LIKE', "%".$genre."%")->first()->id;
        $author = $html->find('[data-event-content=Аберкромби Джо]', 0)->plaintext;
        $book_author = Author::where('name', 'LIKE', "%".$author."%")->first();

        $pages = $html->find('[data-event-label=readingDays]', 0)->getAttribute('data-pages');

        $isbn = Book::max('isbn') + 1;

        $book = Book::create(
            [
                'title' => $title,
                'isbn' => $isbn,
                'description' => $description,
                'price' => (int)$price,
                'number_of_copies' => $quantity,
                'cover_image' => $image,
                'publish_year' => $year,
                'publisher_id' => $idPublisher,
                'category_id' => $idGenre,
                'number_of_pages' => $pages
        ]);

        $book->authors()->attach($book_author);
        $book->save();
    }
    function curlGetPage($url, $referer = 'https://google.com/')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }



}
