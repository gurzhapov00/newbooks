<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ParserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Parser', 'App\Service\ParserService');
    }
}
