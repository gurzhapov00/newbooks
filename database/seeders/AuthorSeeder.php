<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Author;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Author::create(['name' => 'Фатима Хешия']);
        Author::create(['name' => 'Мохамед Ораби']);
        Author::create(['name' => 'Мохаммед Аль-Зайер']);
        Author::create(['name' => 'Омар ан-Навави']);
        Author::create(['name' => 'Маджед Атви']);
        Author::create(['name' => 'Риад Самер']);
    }
}
